import { GET_ERRORS, CLEAR_ERRORS } from '../constants/ActionTypes';

const initialState = {
  error: ''
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CLEAR_ERRORS:
      return {
        ...state,
        error: ''
      }
    case GET_ERRORS:
      return {
        ...state,
        error: action.payload.response.data
      }
    default:
      return state;
  }
}
