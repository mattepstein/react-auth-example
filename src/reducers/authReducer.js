import { LOADING, FINISHED, SET_CURRENT_USER, USER_REGISTERED, USERS_LOADED } from '../constants/ActionTypes';

const isEmpty = require("is-empty");

const initialState = {
  isAuthenticated: false,
  user: {},
  users: {},
  loading: false,
  registered: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true
      }
    case SET_CURRENT_USER:
      return {
        ...state,
        loading: false,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      }
    case FINISHED:
      return {
        ...state,
        loading: false
      }
    case USER_REGISTERED:
      return {
        ...state,
        registered: true
      }
    case USERS_LOADED:
      return {
        ...state,
        users: action.payload
      }
    default:
      return state;
  }
}
