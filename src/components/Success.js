import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import AppHeaderWrapper from './shared/AppHeaderWrapper';
import LoginForm from './forms/LoginForm';

import {
  AppWrapper,
  Container
} from './styles';

class Success extends Component {

  componentDidMount(nextProps) {
    if (!this.props.auth.registered) {
      this.props.history.push("/");
    }
  }

  render() {
    return (
      <AppWrapper>
        <AppHeaderWrapper/>
        <Container>
          <h3>Success!</h3>
          <p>Your account was successfully created. Please login below</p>
          <LoginForm/>
        </Container>
      </AppWrapper>
    );
  }
}

Success.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(withRouter(Success));
