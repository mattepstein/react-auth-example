import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import AppHeaderWrapper from './shared/AppHeaderWrapper'
import Welcome from './Welcome'

import {
  AppWrapper
} from './styles';

class App extends Component {

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/admin");
    }
  }

  render() {
    return (
      <AppWrapper>
        <AppHeaderWrapper/>
        <Welcome/>
      </AppWrapper>
    );
  }
}

App.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(App);
