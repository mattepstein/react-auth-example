import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  InputWrapper
} from '../styles';

class TextInput extends Component {

  static propTypes = {
    type: PropTypes.string,
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    valid: PropTypes.bool,
    touchd: PropTypes.bool,
    label: PropTypes.string,
    className: PropTypes.string,
  };

  static defaultProps = {
    type: 'text',
    value: '',
    valid: false,
    touched: false,
    label: '',
    className: '',
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value || '',
      activeClass: ''
    };
  }

  onChange = event => {
    const { id } = this.props;
    const value = event.target.value;
    this.setState({ value, error: '' });
    return this.props.onChange(id, value);
  }

  _onFocus = event => {
    this.setState({ activeClass: 'active' })
  }

  _onBlur = event => {
    this.setState(!event.target.value ? { activeClass: '' } : { activeClass: 'active'})
  }

  render() {
    const { value } = this.state;
    const { id, className, type, label, valid, touched } = this.props;

    const errorClass = !valid && touched ? 'invalid' : '';

    return (
      <InputWrapper className={`input-field ${className}`}>
        <input
          id={id}
          name={id}
          type={type}
          value={value}
          className={errorClass}
          onChange={this.onChange}
          onFocus={this._onFocus}
          onBlur={this._onBlur}
        />
        <label className={this.state.activeClass} htmlFor={id}>{label}</label>
      </InputWrapper>
    );
  }
};

export default TextInput;
