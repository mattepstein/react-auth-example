import React from 'react';

import loadingGif from '../../assets/loading.gif';

import {
} from '../styles';


const Loader = () => (
  <div><img src={loadingGif} width="40" height="40" alt="loading"/></div>
);

export default Loader;
