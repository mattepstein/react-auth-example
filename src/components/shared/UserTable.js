import React, { Component } from 'react';

import {
} from '../styles';


class UserTable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      users: props.users || []
    };
  }

  renderTableData() {
    return [...this.state.users].reverse().map((user, index) => {
      const { id, username, firstName, lastName } = user //destructuring
      return (
        <tr key={id}>
          <td>{username}</td>
          <td>{firstName} {lastName}</td>
        </tr>
      )
    })
  }

  renderTableHead() {
    return [
        <tr key="12314243">
          <th>Username</th>
          <th>Name</th>
        </tr>
     ]
  }

  render () {
    return (
      <table className="striped">
        <thead>
          {this.renderTableHead()}
        </thead>
        <tbody>
          {this.renderTableData()}
        </tbody>
      </table>
    );
  }
}

export default UserTable;
