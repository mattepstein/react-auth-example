const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const validate = (value, rules) => {
  let isValid = true;

  for (let rule in rules) {
    switch (rule) {
        case 'isRequired': isValid = isValid && requiredValidator(value); break;
        case 'isEmail': isValid = isValid && emailValidator(value); break;
        default: isValid = true;
    }
  }
  return isValid;
}

const requiredValidator = value => value.trim() !== '';
const emailValidator = value => emailRegex.test(String(value).toLowerCase());

export default validate;
