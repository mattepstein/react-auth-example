import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from "../../actions/authActions";
import { Link } from 'react-router-dom';
import logo from '../../assets/logo.svg';
import {
  AppHeader,
  ProfileLinks,
  AppLogo,
  StyledLink
} from '../styles';

class AppHeaderWrapper extends Component {

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const { isAuthenticated, user } = this.props.auth;

    return (
      <AppHeader>
        <Link to="/"><AppLogo src={logo} alt="Logo" /></Link>
        {isAuthenticated &&
          <ProfileLinks>
            <h6>Welcome {user.firstName}!</h6>
            <StyledLink
              to=""
              onClick={this.onLogoutClick}
              className="red-text">Logout</StyledLink>
          </ProfileLinks>
        }
      </AppHeader>
    );
  }
}

AppHeaderWrapper.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(AppHeaderWrapper);
