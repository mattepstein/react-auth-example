import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import { Link } from 'react-router-dom';

import AppHeaderWrapper from './shared/AppHeaderWrapper';
import LoginForm from './forms/LoginForm';

import {
  AppWrapper,
  Container,
  BackLink
} from './styles';

class Login extends Component {
  goBackLink() {
    this.props.history.goBack()
  }

  render() {
    return (
      <AppWrapper>
        <AppHeaderWrapper/>
        <BackLink>
          <Link to='' onClick={this.goBackLink.bind(this)} className="left">&lt; Go Back</Link>
        </BackLink>
        <Container>
          <h3>Login to your account</h3>
          <p>Don't have an account? <Link to='/register'>Register here</Link></p>
          <LoginForm/>
        </Container>
      </AppWrapper>
    );
  }
}

export default withRouter(Login);
