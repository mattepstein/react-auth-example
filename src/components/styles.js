import styled from "styled-components";
import { createGlobalStyle } from 'styled-components'
import { Link } from 'react-router-dom'

// Theme colors
const white = '#fff',
      black = '#000',
      textBlack = '#101010',
      textWhite = '#d2dee5',
      lightGray = '#adafb2',
      gray = '#717376',
      darkGray = '#252525',
      bgBlueHighlight = '#183e56',
      bgBlue = '#07151e',
      primaryRed = '#c9343a',
      secondaryBlue = '#7ea8b6',
      primaryRedDarker = '#a01c22';

// Theme spacing standards
const paddingVal = '20px';

export const GlobalStyle = createGlobalStyle`
  html,
  body,
  #root {
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
    color: ${textWhite};
    font-size: 14px;
    font-weight: 300;
  }
  h1, h2, h3, h4, h5, h6{
    color: ${white};
  }
  a{
    text-decoration: none;
    color: ${primaryRed};
  }
  fieldset{ border: none }

  body{
    background: ${bgBlue};
  }
  table.striped th{
    color: ${secondaryBlue};
  }
  table.striped>tbody>tr:nth-child(odd){
    background-color: ${bgBlueHighlight};
  }
`

export const Container = styled.div `
  max-width: 480px;
  width: 100%;
  margin: 60px auto 0;

  @media(max-width: 768px) {
    margin: 0 20px;
    width: auto;
  }
`

export const ContainerLg = styled(Container)`
  max-width: 600px;

  h3 + p{
    margin-bottom: 40px;
  }

  @media(max-width: 768px) {
    margin-top: 40px;
  }
`;

export const AppWrapper = styled.div `
  text-align: center;
  display: flex;
  flex-direction: column;
`;

export const AppHeader = styled.div `
  text-align: left;
  background-color: ${black}
  padding: ${paddingVal}
  min-height: 10vh;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  color: white;

  @media(max-width: 768px) {
    justify-content: center;
  }
`

export const AppLogo = styled.img `
  height: 3em;
  display: block;
`

export const ProfileLinks = styled.div `
  align-self: content-end;
  text-align: right;

  @media(max-width: 768px) {
    position: absolute;
    right: 10px;
    top: 10px;
    padding: 5px;

    h6{
      display: none;
    }
  }
`

export const InputWrapper = styled.div `
  input[type=text],
  input[type=email],
  input[type=password]{
    border-color: ${gray};
    color: ${textWhite};

    &:focus:not(.browser-default):focus:not([readonly]){
      border-bottom-color: ${secondaryBlue};
      box-shadow: 0 1px 0 0 ${secondaryBlue};
    }
  }
`

export const StyledLink = styled(Link)`
  font-weight: bold;
  margin: 0 5px;

  &.btn-primary{
    background-color: ${primaryRed};
    border-color: ${primaryRed};
    color: ${textWhite};

    &:hover{
      background-color: ${primaryRedDarker};
      border-color: ${primaryRedDarker};
    }
  }

  &.btn-secondary{
    background-color: ${gray};
    border-color: ${gray};
    color: ${textWhite};

    &:hover{
      background-color: ${darkGray};
      border-color: ${darkGray};
    }
  }
`

export const Button = styled.button `
  font-weight: bold;

  &[disabled][disabled]{
    opacity: .5;
    cursor: not-allowed;
  }
`

export const PrimaryButton = styled(Button) `
  background-color: ${primaryRed};
  border-color: ${primaryRed};
  color: ${textWhite};

  &:hover{
    background-color: ${primaryRedDarker};
    border-color: ${primaryRedDarker};
  }
 `

 export const SecondaryButton = styled(Button) `
  background-color: ${gray};
  border-color: ${gray};
  color: ${textWhite};

  &:hover{
    background-color: ${darkGray};
    border-color: ${darkGray};
  }
`

export const BackLink = styled.div`
  display: inline-block;
  float: left;
  margin: 20px;
`
;
