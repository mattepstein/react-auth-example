import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getAllUsers } from "../../actions/authActions";

import AppHeaderWrapper from '../shared/AppHeaderWrapper';
import UserTable from '../shared/UserTable';
import Loader from '../shared/Loader';

import {
  AppWrapper,
  Container
} from '../styles';


class Admin extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      tableLoading: true
    }
  }

  componentDidMount() {
    this.props.getAllUsers();
  }

  componentDidUpdate (nextState) {
    if(nextState.users !== this.props.users) {
      this.setState({users: this.props.users, tableLoading: false})
    }
  }

  render () {
    return (
      <AppWrapper>
        <AppHeaderWrapper/>
        <Container>
          <h3>Users Listing</h3>
          {!this.state.tableLoading ? <UserTable users={this.state.users}/> : <Loader/>}
        </Container>
      </AppWrapper>
    );
  }
}

Admin.propTypes = {
  getAllUsers: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  error: PropTypes.object
};

Admin.defaultProps = {
  errors: {}
};

const mapStateToProps = state => ({
  errors: state.error,
  users: state.auth.users
});

export default connect(
  mapStateToProps,
  { getAllUsers }
)(withRouter(Admin));
