import React, {Component} from 'react';
import { withRouter } from "react-router-dom";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { registerUser } from "../../actions/authActions";

import validate from '../shared/validate';
import TextInput from '../shared/TextInput';
import Loader from '../shared/Loader';

import {
  PrimaryButton
} from '../styles';

class RegisterForm extends Component {
  constructor() {
    super();
    this.state = {
      validForm: false,
      fields: {
        firstName: {
          value: '',
          type: 'text',
          placeholder: 'First Name',
          valid: false,
          validationRules: {
            isRequired: true
          },
          touched: false
        },
        lastName: {
          value: '',
          type: 'text',
          placeholder: 'Last Name',
          valid: false,
          validationRules: {
            isRequired: true
          },
          touched: false
        },
        username: {
          value: '',
          type: 'email',
          placeholder: 'Email Address',
          valid: false,
          validationRules: {
            isRequired: true,
            isEmail: true
          },
          touched: false
        },
        password: {
          value: '',
          type: 'password',
          placeholder: 'Password',
          valid: false,
          validationRules: {
            isRequired: true
          },
          touched: false
        }
      }
    };
  }

  onChange = (name, value) => {
    const updatedFields = {
      ...this.state.fields
    };
    const updatedInput = {
      ...updatedFields[name]
    };
    updatedInput.value = value;
    updatedInput.touched = true;
    updatedInput.valid = validate(value, updatedInput.validationRules);

    updatedFields[name] = updatedInput;

    let validForm = true;
    for (let inputIdentifier in updatedFields) {
      validForm = updatedFields[inputIdentifier].valid && validForm;
    };

    this.setState({
      fields: updatedFields,
      validForm: validForm
    })
  }

  onSubmit = e => {
    e.preventDefault();

    if(this.state.validForm) {
      const formData = {};

      for (let fieldId in this.state.fields) {
        formData[fieldId] = this.state.fields[fieldId].value;
      }

      this.props.registerUser(formData, this.props.history);
    }
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/admin");
    }
  }

  render () {
    const formError = this.props.errors.error,
          loading = this.props.auth.loading;

    return (
      <form noValidate onSubmit={this.onSubmit}>
        <fieldset className="row">
          <TextInput
            id="firstName"
            label={this.state.fields.firstName.placeholder}
            className="col s12 l12"
            touched={this.state.fields.firstName.touched}
            valid={this.state.fields.firstName.valid}
            onChange={this.onChange}
          />
          <TextInput
            id="lastName"
            label={this.state.fields.lastName.placeholder}
            className="col s12 l12"
            onChange={this.onChange}
            touched={this.state.fields.lastName.touched}
            valid={this.state.fields.lastName.valid}
          />
          <TextInput
            id="username"
            label={this.state.fields.username.placeholder}
            type="email"
            className="col s12 l12"
            onChange={this.onChange}
            touched={this.state.fields.username.touched}
            valid={this.state.fields.username.valid}
          />
          <TextInput
            id="password"
            label={this.state.fields.password.placeholder}
            type="password"
            className="col s12 l12"
            onChange={this.onChange}
            touched={this.state.fields.password.touched}
            valid={this.state.fields.password.valid}
          />
        </fieldset>
        {formError && <p className="red-text text-accent-4">{formError}</p>}
        {loading && <Loader/>}
        <PrimaryButton disabled={!this.state.validForm || loading} type="submit" className="btn-large btn-flat">Create Account</PrimaryButton>
      </form>
    );
  }
}

RegisterForm.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object
};

RegisterForm.defaultProps = {
  auth: {},
  errors: {}
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.error
});

export default connect(
  mapStateToProps,
  { registerUser }
)(withRouter(RegisterForm));
