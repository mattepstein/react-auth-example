import React from 'react';

import {
  StyledLink,
  ContainerLg
} from './styles';

const Welcome = () => (
  <ContainerLg>
    <h3>React Auth App</h3>
    <p>Thanks for stopping by!</p>
    <StyledLink className="btn btn-large btn-flat btn-primary" to="/login">Login</StyledLink>
    <StyledLink className="btn btn-large btn-flat btn-secondary" to="/register">Create Account</StyledLink>
  </ContainerLg>
);

export default Welcome;
