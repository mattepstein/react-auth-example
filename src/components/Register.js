import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import { Link } from 'react-router-dom';

import AppHeaderWrapper from './shared/AppHeaderWrapper';
import RegisterForm from './forms/RegisterForm';

import {
  AppWrapper,
  Container,
  BackLink
} from './styles';

class Register extends Component {
  goBackLink() {
    this.props.history.goBack()
  }

  render() {
    return (
      <AppWrapper>
        <AppHeaderWrapper/>
        <BackLink>
          <Link to='' onClick={this.goBackLink.bind(this)} className="left">&lt; Go Back</Link>
        </BackLink>
        <Container>
          <h3>Register an account</h3>
          <p>Already have an account? <Link to='/login'>Login here</Link></p>
          <RegisterForm/>
        </Container>
      </AppWrapper>
    );
  }
}

export default withRouter(Register);
