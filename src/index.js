import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { KJUR, b64utoutf8 } from 'jsrsasign';
import configureStore from './store/configureStore';
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser } from "./actions/authActions";

// Components
import App from './components/App';
import Login from './components/Login';
import Register from './components/Register';
import PrivateRoute from "./components/private/PrivateRoute";
import Admin from './components/admin/Admin';
import Success from './components/Success';

import {
  GlobalStyle,
} from './components/styles';

const cacheStore = window.localStorage.getItem('store') || {};
const store = configureStore(cacheStore);

if (localStorage.jwtToken) {
  const token = localStorage.jwtToken;
  setAuthToken(token);

  let decoded = KJUR.jws.JWS.readSafeJSONString(b64utoutf8(token.split(".")[1]));

  store.dispatch(setCurrentUser(decoded));

}

render(
  <Provider store={store}>
    <React.Fragment>
      <GlobalStyle />
      <Router>
        <Switch>
          <Route exact path="/" component={App} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/success" component={Success} />
        </Switch>
        <Switch>
          <PrivateRoute exact path="/admin" component={Admin} />
        </Switch>
      </Router>
    </React.Fragment>
  </Provider>,
  document.getElementById('root'),
);
