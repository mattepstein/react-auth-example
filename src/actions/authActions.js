import axios from "axios";
import { KJUR } from 'jsrsasign';
import setAuthToken from "../utils/setAuthToken";
import * as config from '../constants/config';

import { GET_ERRORS, CLEAR_ERRORS, SET_CURRENT_USER, LOADING, FINISHED, USER_REGISTERED, USERS_LOADED } from "../constants/ActionTypes";

// Register User
export const registerUser = (formData, history) => dispatch => {
  const url = `${config.API_HOST}/user`,
        data = JSON.stringify({
          ...formData,
          mobilePhone: 'n/a'
        });

  dispatch(setLoading());
  dispatch(clearErrors());

  axios
    .post(url, data)
    .then(res => {
      dispatch(setFinished());
      dispatch(setRegistered());
      history.push("/success");
    })
    .catch(err => {
      dispatch(setErrors(err));
      dispatch(setFinished());
    });
};

// Login - get user token
export const loginUser = (formData, history) => dispatch => {
  let url = `${config.API_HOST}/user/${formData.username}/${formData.password}`;

  dispatch(setLoading());
  dispatch(clearErrors());

  axios
    .get(url)
    .then(res => {
      const header = {alg: "HS256", typ: "JWT"};
      const payload = res.data;
      const jwtToken = KJUR.jws.JWS.sign("HS256", header, payload, {utf8: "secret"});

      localStorage.setItem("jwtToken", jwtToken);
      setAuthToken(jwtToken);

      dispatch(setCurrentUser(res.data));
      history.push("/")
    })
    .catch(err => {
        dispatch(setErrors(err));
        dispatch(setFinished());
      }
    );
};

export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

export const setLoading = () => {
  return {
    type: LOADING
  };
};

export const setErrors = err => {
  return {
    type: GET_ERRORS,
    payload: err
  }
}

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const setFinished = () => {
  return {
    type: FINISHED
  };
};

export const setRegistered = () => {
  return {
    type: USER_REGISTERED
  };
};

export const setUsersLoaded = users => {
  return {
    type: USERS_LOADED,
    payload: users
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  localStorage.removeItem("jwtToken");
  setAuthToken(false);
  dispatch(setCurrentUser({}));
};

export const getAllUsers = () => dispatch => {
  const url = `${config.API_HOST}/user`;

  axios
    .get(url)
    .then(res => {
      dispatch(setUsersLoaded(res.data));
    })
    .catch(err => {
        dispatch(setErrors(err));
        dispatch(setFinished())
      }
    );
};
