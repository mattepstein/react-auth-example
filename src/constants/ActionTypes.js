export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const LOADING = "LOADING";
export const FINISHED = "FINISHED";
export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const USER_REGISTERED = "USER_REGISTERED";
export const USERS_LOADED = "USERS_LOADED";
